import {ref, watchEffect} from "vue";
import {projectFirestore} from "@/firebase/config";

const getDocument = (collection, id) => {
  const document = ref({})
  const error = ref(null)

  let documentRef = projectFirestore.collection(collection).doc(id)

  const unsub = documentRef.onSnapshot((docSnap) => {
    if(docSnap.data()) {
      document.value = {...docSnap.data(), docId: docSnap.id}
      error.value = null
    }else{
      error.value = 'That document does not exist'
    }
  }, (err) => {
    console.log(err.message)
    error.value = 'could not fetch document'
  })

  watchEffect((onInvalidate) => {
    onInvalidate(() => unsub())
  })

  return {error, document}

}

export default getDocument