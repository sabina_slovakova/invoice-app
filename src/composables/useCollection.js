import {ref} from "vue";
import {projectFirestore} from "@/firebase/config";

const addCollection = (collection) => {
  const error = ref(null)

  const addDoc = async (doc) => {
    error.value = null
    try {
      return await projectFirestore.collection(collection).add(doc)
    }catch (err){
      error.value = err.message
      console.log(error.value)
    }

  }

  return { error, addDoc }
}

export default addCollection