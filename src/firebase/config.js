import firebase from 'firebase/app'
import 'firebase/firestore'



const firebaseConfig = {
  apiKey: "AIzaSyDWgJfvKw4pL9WvLfau7hYU0Ze9nAC8sUs",
  authDomain: "invoice-app-fm-2a873.firebaseapp.com",
  projectId: "invoice-app-fm-2a873",
  storageBucket: "invoice-app-fm-2a873.appspot.com",
  messagingSenderId: "80634645934",
  appId: "1:80634645934:web:2e49bac390ce88bd7a4c3f"
};

//init firebase back-end
firebase.initializeApp(firebaseConfig)

//init services
const projectFirestore = firebase.firestore()


//timestamp
const timestamp = firebase.firestore.FieldValue.serverTimestamp

export { projectFirestore, timestamp }