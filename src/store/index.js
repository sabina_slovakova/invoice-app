import { createStore } from "vuex";
import {projectFirestore} from "@/firebase/config";

export default createStore({
  state: {
    invoicesData: [],
    invoiceForm: null,
    statusFilter: false,
    isInvoiceModalActive: false,
    isPopupActive: false,
    invoicesPending: null,
    isInvoiceEditing: null,
    currentInvoiceId:'',
    currentInvoiceArray: [],
  },
  getters: {
    getNumberOfInvoices: state => {
      return state.invoicesData.length
    }
  },
  mutations: {
    TOGGLE_INVOICE(state){
      state.invoiceForm = !state.invoiceForm;
    },
    TOGGLE_FILTER(state){
      state.statusFilter = !state.statusFilter
    },
    CLOSE_FILTER(state){
      state.statusFilter = false
    },
    TOGGLE_BACKDROP(state){
      state.isInvoiceModalActive = !state.isInvoiceModalActive
    },
    TOGGLE_POPUP(state){
      state.isPopupActive = !state.isPopupActive
    },
    SET_INVOICE_DATA(state, data) {
      state.invoicesData.push(data)
    },
    INVOICES_PENDING(state) {
      state.invoicesPending = true
    },
    TOGGLE_EDIT_INVOICE(state) {
      state.isInvoiceEditing = !state.isInvoiceEditing
    },
    SET_CURRENT_INVOICE_ID(state, id) {
      state.currentInvoiceId = id
    },
    SET_CURRENT_INVOICE(state, id) {
      state.currentInvoiceArray = state.invoicesData.filter(invoice => {
        return invoice.docId === id
      })
    },
    DELETE_INVOICE(state, id) {
      state.invoicesData = state.invoicesData.filter(invoice => invoice.docId !== id)
    },
    UPDATE_TO_PAID(state, id) {
      state.invoicesData.forEach( inv => {
        if(inv.docId === id) {
          inv.isInvoicePending = false
          inv.isInvoicePaid = true
        }
      })
    },
    UPDATE_TO_PENDING(state, id) {
      state.invoicesData.forEach( inv => {
        if(inv.docId === id) {
          inv.isInvoicePending = true
          inv.isInvoicePaid = false
        }
      })
    },

  },
  actions: {
    async GET_INVOICES({ commit, state }) {
      const res = await projectFirestore.collection("invoices").orderBy('createdAt').get()
      res.docs.forEach((doc) => {
        if (!state.invoicesData.some((invoice) => invoice.docId === doc.id)) {
          const data = { ...doc.data(), docId: doc.id };
          commit("SET_INVOICE_DATA", data);
        }
      });
      commit('INVOICES_PENDING')
    },
    async UPDATE_INVOICE({commit, dispatch}, docId) {
      commit('DELETE_INVOICE',docId)
      await dispatch('GET_INVOICES')
      commit('TOGGLE_INVOICE')
      commit('TOGGLE_BACKDROP')
      commit('TOGGLE_EDIT_INVOICE')
      commit('SET_CURRENT_INVOICE', docId)
    }
  },
  modules: {},
});
