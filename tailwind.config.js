module.exports = {
  purge: { content: ["./public/**/*.html", "./src/**/*.vue"] },
  darkMode: 'class', // or 'media' or 'class'
  theme: {
    screens: {
      'xxs': '450px',
      'xs': '500px',
      's': '550px',
      'sm': '640px',
      'md': '768px',
      'lg': '900px',
      'xlg': '1024px',
      '2xl': '1280px',
      '3xl': '1536px',
    },
    fontSize: {
      'xxs': '.6875rem',
      'xs': '.75rem',
      'sm': '.875rem',
      'base': '1rem',
      'lg': '1.125rem',
      'xl': '1.25rem',
      '2xl': '1.5rem',
      '3xl': '1.875rem',
      '4xl': '2.25rem',
      '5xl': '3rem',
      '6xl': '4rem',
    },
    fill: {
      current: 'currentColor',
      'red': '#ec5757',
    },
    extend: {
      fontFamily: {
        body: ['Spartan', 'sans-serif']
      },
      colors: {
        'primary-100': '#9277ff',
        'primary-200': '#7c5dfa',
        'primary-300': '#7e88c3',
        'secondary-l': '#373b53',
        'secondary-d': '#1e2139',
        'background-l': '#f8f8fb',
        'background-d': '#151725',
        'background-ld': '#252945',
        'background-form-d': '#141625',
        'clr-btn-grey': '#f9fafe',
        'clr-grey-100': '#dfe3fa',
        'clr-grey-200': '#888eb0',
        'clr-btn-dark': '#0c0e16',

      },
      gridTemplateColumns: {
        'custom-3': '1fr 1fr 1fr',
        'ss': '4rem repeat(4, 1fr) min-content',
        'ls': '2.5fr 4.125rem 1.25fr 0.75fr min-content',
        'invoice-layout-lg': '7rem 9rem 1fr min-content min-content min-content'
      },
      gridTemplateRows: {
        'invoice-layout-sm': 'max-content 1fr 1fr',
        'invoice-layout-lg': 'min-content'
        },
      spacing: {
        '31': '32px',
        '29': '29px',
      }
    },
  },
  variants: {
    extend: {
      backgroundColor: ['checked'],
      borderColor: ['checked'],
      fill: ['hover'],
    },
  },
  plugins: [
    require('@tailwindcss/forms'),
  ],
};
